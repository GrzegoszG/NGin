using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Extensions;

public class NPCGfx : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer head;

    [SerializeField]
    private SpriteRenderer leftArm;
    [SerializeField]
    private SpriteRenderer leftArmLower;
    [SerializeField]
    private SpriteRenderer leftHand;

    [SerializeField]
    private SpriteRenderer rightArm;
    [SerializeField]
    private SpriteRenderer rightArmLower;
    [SerializeField]
    private SpriteRenderer rightHand;

    [SerializeField]
    private SpriteRenderer leftLeg;
    [SerializeField]
    private SpriteRenderer leftLegLower;
    [SerializeField]
    private SpriteRenderer leftFoot;

    [SerializeField]
    private SpriteRenderer rightLeg;
    [SerializeField]
    private SpriteRenderer rightLegLower;
    [SerializeField]
    private SpriteRenderer rightFoot;

    [SerializeField]
    private SpriteRenderer torso;

    private Dictionary<NPCBodyPart, SpriteRenderer> sprites;

    void Start()
    {
        LoadBodyParts();
    }

    private void LoadBodyParts()
    {
        sprites = new Dictionary<NPCBodyPart, SpriteRenderer>();
        //var enums = System.Enum.GetValues(typeof(NPCBodyPart)).OfType<NPCBodyPart>().ToList();

        sprites.Set(NPCBodyPart.Head, head);

        sprites.Set(NPCBodyPart.Torso, torso);

        sprites.Set(NPCBodyPart.LeftArm, leftArm);
        sprites.Set(NPCBodyPart.LeftArmLower, leftArmLower);
        sprites.Set(NPCBodyPart.LeftHand, leftHand);
        
        sprites.Set(NPCBodyPart.RightArm, rightArm);
        sprites.Set(NPCBodyPart.RightArmLower, rightArmLower);
        sprites.Set(NPCBodyPart.RightHand, rightHand);
        
        sprites.Set(NPCBodyPart.LeftLeg, leftLeg);
        sprites.Set(NPCBodyPart.LeftLegLower, leftLegLower);
        sprites.Set(NPCBodyPart.LeftFoot, leftFoot);

        sprites.Set(NPCBodyPart.RightLeg, rightLeg);
        sprites.Set(NPCBodyPart.RightLegLower, rightLegLower);
        sprites.Set(NPCBodyPart.RightFoot, rightFoot);
    }

    public enum NPCBodyPart
    {
        Head,
        LeftArm,
        LeftArmLower,
        LeftHand,
        RightArm,
        RightArmLower,
        RightHand,
        LeftLeg,
        LeftLegLower,
        LeftFoot,
        RightLeg,
        RightLegLower,
        RightFoot,
        Torso
    }
}
