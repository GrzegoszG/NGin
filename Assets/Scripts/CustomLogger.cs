﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CustomLogger : MonoBehaviour
    {
        public void Log(object message, Object context = null)
        {
            if(context == null)
            {
                Debug.Log(message);
            }
            else
            {
                Debug.Log(message, context);
            }
        }
    }
}
