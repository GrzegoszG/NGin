﻿using Assets.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Scripts
{
    public class NPCPathfindingProvider
    {
        private GameObject _targetPrefab;
        private Transform _parent;

        public NPCPathfindingProvider(GameObject targetPrefab, Transform parent)
        {
            _targetPrefab = targetPrefab;
            _parent = parent;
        }

        public Transform GetNewPathfindingTarget(IMovable movable)
        {
            var obj = GameObject.Instantiate(_targetPrefab, _parent);
            return obj.transform;
        }
    }
}
