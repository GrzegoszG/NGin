﻿using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/NPCStats", order = 2)]
    public class NPCStats : ScriptableObject
    {
        public float Speed;
        public float FleeDistance;
        public float FleeSpeedMult;
        public float FleeMaxAngle;
        public float MinFleeDistance;
        public float MaxFleeDistance;
        public float CurioInteractionDistance;
    }
}
