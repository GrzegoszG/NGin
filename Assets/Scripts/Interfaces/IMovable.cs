﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IMovable
    {
        Mover Mover { get; set; }
        Transform Target { get; set; }
    }
}
