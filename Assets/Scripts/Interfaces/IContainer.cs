﻿using Assets.Scripts.Entities.Items;
using System.Collections.Generic;

namespace Assets.Scripts.Interfaces
{
    public interface IContainer
    {
        List<IItem> GetItems();
        bool PutItem(IContainer who, IItem item);
        bool TakeItem(IContainer who, IItem item);
    }
}
