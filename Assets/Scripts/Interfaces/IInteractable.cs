﻿using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    public interface IInteractable : ILoggable
    {
        bool ReadyToUse { get; }
        void Interact(IInteractor interactor);
    }
}
