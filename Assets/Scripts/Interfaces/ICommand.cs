﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public interface ICommand
    {
        void Execute();
        bool IsFinished { get; }
        float Duration { get; set; }
        float RemainingTime { get; }
    }
}
