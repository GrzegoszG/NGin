﻿namespace Assets.Scripts.Interfaces
{
    public interface ILoggable
    {
        CustomLogger Logger { get; }
    }
}
