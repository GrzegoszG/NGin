﻿using System;
namespace Assets.Scripts.Interfaces
{
    public interface IStateMachine<State> where State : IState
    {
        State CurrentState { get; }
        State PreviousState { get; }
        State InitialState { get; }

        void Tick();
        void SetState(State state);
        void AddTransition(State from, State to, Func<bool> predicate);
        void AddAnyTransition(State state, Func<bool> predicate);
    }
}
