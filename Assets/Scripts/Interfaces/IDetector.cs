﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public interface IDetector
    {
        List<Actor> Detected { get; set; }
        void OnDetect(Actor actor);
        void OnLose(Actor actor);
    }
}
