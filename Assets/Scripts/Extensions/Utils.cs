﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public static class Utils
    {
        /// <summary>
        /// Adds a component of type T or returns existing if one exists already
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="go"></param>
        /// <returns></returns>
        public static T AddComponentIfNull<T>(this GameObject go) where T : Component
        {
            if(go.TryGetComponent<T>(out var c))
            {
                return c;
            }
            else
            {
                var comp = go.AddComponent<T>();
                return comp;
            }
        }

        /// <summary>
        /// Destroys existing component of type T if any, then creates a new one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="go"></param>
        /// <returns></returns>
        public static T AddComponentForce<T>(this GameObject go) where T : Component
        {
            if (go.TryGetComponent<T>(out var c))
            {
                GameObject.Destroy(c);
            }
            var comp = go.AddComponent<T>();
            return comp;
        }

        public static CustomLogger CreateLogger(this GameObject go, bool setEnabledTo = false)
        {
            var logger = go.AddComponentIfNull<CustomLogger>();
            logger.enabled = setEnabledTo;
            return logger;
        }

        public static void Set<K, V>(this Dictionary<K, V> dic, K key, V value)
        {
            if (dic == null) return;

            if(dic.ContainsKey(key))
            {
                dic[key] = value;
            }
            else
            {
                dic.Add(key, value);
            }
        }
    }
}
