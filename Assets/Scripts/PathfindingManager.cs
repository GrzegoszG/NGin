using UnityEngine;
using Pathfinding;
using Assets.Scripts;
using Assets.Scripts.Interfaces;

[RequireComponent(typeof(AstarPath))]
public class PathfindingManager : MonoBehaviour
{
    [SerializeField]
    private GameObject targetPrefab;

    private AstarPath _astarPath;
    private NavGraph _mainGraph => _astarPath.graphs.Length > 0 ? _astarPath.graphs[0] : null;
    private static NPCPathfindingProvider _pp;

    void Awake()
    {
        _astarPath = GetComponent<AstarPath>();
        _pp = new NPCPathfindingProvider(targetPrefab, transform);
    }

    public void Rescan()
    {
        if(_mainGraph != null)
            _astarPath.Scan(_mainGraph);
    }

    public static Transform GetPathfindingTargetFor(IMovable movable)
    {
        return _pp.GetNewPathfindingTarget(movable);
    }
}
