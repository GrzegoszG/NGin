﻿using UnityEngine;

namespace Assets.Scripts.Templates
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/NPC", order = 1)]
    public class NPCTemplate : ScriptableObject
    {
        public string Name;
        public NPCStats Stats;
    }
}
