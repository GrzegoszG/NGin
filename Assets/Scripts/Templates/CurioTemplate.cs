using UnityEngine;

namespace Scripts.Templates
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Curio", order = 1)]
    public class CurioTemplate : ScriptableObject
    {
        public Sprite Sprite;
    }
}