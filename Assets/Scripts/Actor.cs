using Assets.Scripts;
using Assets.Scripts.Extensions;
using Assets.Scripts.Interfaces;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public abstract class Actor : MonoBehaviour, IInteractable, IInteractor, IMovable
{
    #region Properties

    public string Name { get; internal set; }
    public float Speed { get; internal set; }

    private Identity _identity;
    public Identity Identity
    {
        get
        {
            return _identity;
        }
    }

    public string InteractorName { get => gameObject.name; }
    public Mover Mover { get; set; }
    public Transform Target { get; set; }

    private CustomLogger _logger;
    public CustomLogger Logger => _logger;
    public bool ReadyToUse { get; private set; }

    #endregion

    private Rigidbody2D rb;

    #region Interface implementation

    public void InteractWith(IInteractable interactable)
    {
        interactable.Interact(this);
    }

    public void Interact(IInteractor interactor)
    {
        if (!ReadyToUse) return;
    }

    #endregion

    #region Methods

    public void InitComponents()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        _logger = gameObject.CreateLogger();
    }

    #endregion
}
