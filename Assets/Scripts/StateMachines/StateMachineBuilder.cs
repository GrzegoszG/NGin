﻿using Assets.Scripts.States;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class StateMachineBuilder
    {
        private readonly NPC _npc;
        public StateMachineBuilder(NPC npc)
        {
            _npc = npc;
        }

        public StateMachine Build(bool setInitialState = true)
        {
            var idle = new Idle();
            var attack = new Attack();
            var move = new MoveToTarget(_npc.Mover);
            var flee = new Flee(_npc);
            var curioSearch = new SearchForCurio(_npc);
            var use = new UseCurio(_npc);
            var routine = new Routine(_npc, new List<ScheduleItem> 
                {
                    new RotateScheduleItem(_npc.transform, true).WithDuration(2).WithLocation(new UnityEngine.Vector2(-20, -10)),
                    new RotateScheduleItem(_npc.transform, false).WithDuration(2).WithLocation(new UnityEngine.Vector2(-10, -10))
                });
            var sm = new StateMachine(curioSearch);

            sm.AddAnyTransition(flee, flee.IsScaredCondition());
            //sm.AddAnyTransition(attack, _npc.IsAngry());
            sm.AddTransition(curioSearch, move, _npc.HasInteractionTarget());
            sm.AddTransition(move, use, _npc.HasInteractionTarget());
            sm.AddAnyTransition(move, _npc.HasTarget());
            sm.AddAnyTransition(curioSearch, _npc.HasNoInteractionTarget());
            sm.AddAnyTransition(routine, () => true);

            if(setInitialState)
                sm.SetState(sm.InitialState);

            return sm;
        }

        public StateMachine BuildMoveStateMachine()
        {
            var idle = new Idle();
            var move = new MoveToTarget(_npc.Mover);
            var sm = new StateMachine(idle);

            sm.AddAnyTransition(move, _npc.HasTarget());
            sm.AddAnyTransition(idle, () => true);

            return sm;
        }
    }
}
