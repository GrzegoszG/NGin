﻿using Assets.Scripts.States;
using System;

namespace Assets.Scripts.StateMachines
{
    public class MoveStateMachine : StateMachine
    {
        private readonly Idle _idle;
        private readonly MoveToTarget _move;
        public MoveStateMachine(MoveStateMachineArgs args) : base(new Idle())
        {
            _idle = (Idle)InitialState;
            _move = new MoveToTarget(args.Mover);
            AddAnyTransition(_move, args.Condition);
            AddAnyTransition(_idle, () => true);
            SetState(_idle);
        }

        public void SetMoveToState()
        {
            SetState(_move);
        }
    }

    public class MoveStateMachineArgs
    {
        public Mover Mover { get; }
        public Func<bool> Condition { get; }

        public MoveStateMachineArgs(Mover mover, Func<bool> condition)
        {
            Mover = mover;
            Condition = condition;
        }
    }
}
