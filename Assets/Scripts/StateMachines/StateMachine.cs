﻿using Assets.Scripts;
using Assets.Scripts.Interfaces;
using Assets.Scripts.States;
using System;
using System.Collections.Generic;
using System.Linq;

public abstract class BaseStateMachine<IS> : IStateMachine<IS> where IS : IState
{
    protected IS _currentState;
    public IS CurrentState => _currentState;

    public IS PreviousState { get; private set; }

    protected IS _initialState;
    public IS InitialState
    {
        get
        {
            if (_initialState == null)
                _initialState = _anyTransitions.FirstOrDefault().To;
            return _initialState;
        }
    }
    protected Dictionary<Type, List<Transition>> _transitions = new Dictionary<Type, List<Transition>>();
    protected List<Transition> _currentTransitions = new List<Transition>();
    protected List<Transition> _anyTransitions = new List<Transition>();

    protected static List<Transition> EmptyTransitions = new List<Transition>(0);
    protected StateChangedArgs _changedArgs;

    public event CustomDelegate.StateChangedDelegate OnStateChanged;

    public BaseStateMachine(IS initialState)
    {
        _initialState = initialState;
        _changedArgs = new StateChangedArgs();
    }

    public void Tick()
    {
        var transition = GetTransition();
        if (transition != null)
            SetState(transition.To);

        _currentState?.Tick();
    }

    public void SetState(IS state)
    {
        if (state.Equals(_currentState))
            return;

        SaveChange(_currentState, state);

        _currentState?.OnExit();
        _currentState = state;

        if(!_transitions.TryGetValue(_currentState.GetType(), out _currentTransitions))
            _currentTransitions = EmptyTransitions;

        _currentState.OnEnter();
        OnStateChanged?.Invoke(_changedArgs);
    }

    protected void SaveChange(IS oldState, IS newState)
    {
        PreviousState = oldState;
        _changedArgs.OldState = oldState?.GetType().Name ?? "";
        _changedArgs.NewState = newState?.GetType().Name ?? "";
    }

    public void AddTransition(IS from, IS to, Func<bool> predicate)
    {
        if (_transitions.TryGetValue(from.GetType(), out var transitions) == false)
        {
            transitions = new List<Transition>();
            _transitions[from.GetType()] = transitions;
        }

        transitions.Add(new Transition(to, predicate));
    }

    public void AddAnyTransition(IS state, Func<bool> predicate)
    {
        _anyTransitions.Add(new Transition(state, predicate));
    }

    protected class Transition
    {
        public Func<bool> Condition { get; }
        public IS To { get; }

        public Transition(IS to, Func<bool> condition)
        {
            To = to;
            Condition = condition;
        }
    }

    protected Transition GetTransition()
    {
        foreach (var transition in _anyTransitions)
            if (transition.Condition())
                return transition;

        foreach (var transition in _currentTransitions)
            if (transition.Condition())
                return transition;

        return null;
    }
}

public class StateMachine : BaseStateMachine<AIState>
{
    public StateMachine(AIState initialState) : base(initialState)
    {

    }
}