﻿using Assets.Scripts.StateMachines;
using Assets.Scripts.States;

namespace Assets.Scripts
{
    public class RoutineMachine : BaseStateMachine<ScheduleItem>
    {
        private MoveStateMachine _moveStateMachine;
        private readonly MoveToTarget _moveState;
        private readonly Mover _mover;
        private readonly Idle _idle;
        public float MinDistanceToNext { get; set; }
        public bool IsMovingToNext { get; private set; }

        public RoutineMachine(ScheduleItem initialState, Mover mover) : base(initialState)
        {
            _idle = new Idle();
            _mover = mover;
            _moveState = new MoveToTarget(_mover);
            _moveStateMachine = new MoveStateMachine(new MoveStateMachineArgs(_mover, MoverCondition));
            
            if (MinDistanceToNext <= 0)
                MinDistanceToNext = 1f;
        }

        public new void SetState(ScheduleItem state)
        {
            base.SetState(state);
        }

        private bool _isReadyToChangeState;
        public new void Tick()
        {
            _isReadyToChangeState = _currentState?.Finished() ?? false;
            if (_isReadyToChangeState)
            {
                _moveStateMachine.SetState(_moveStateMachine.InitialState);

                if (_currentState.Next.Position.HasValue)
                {
                    if (IsAwayFromNext())
                    {
                        if (_moveStateMachine.CurrentState is Idle)
                        {
                            _mover.Target.position = _currentState.Next.Position.Value;
                            _moveStateMachine.SetMoveToState();
                        }
                        else
                        {
                            _moveStateMachine.Tick();
                        }
                        return;
                    }
                    else
                    {
                        SetState(_currentState.Next);
                    }
                }
            }
            _currentState?.Tick();
        }

        private System.Func<bool> MoverCondition => () => IsAwayFromNext();

        private bool IsAwayFromNext()
        {
            bool needsToMove = GetDistanceFromNextState() > MinDistanceToNext;
            return needsToMove;
        }

        private float GetDistanceFromNextState()
        {
            if(_currentState?.Next?.Position.HasValue ?? false)
            {
                var diff = _currentState.Next.Position.Value - _mover.Position;
                return diff.magnitude;
            }
            return 0;
        }
    }
}
