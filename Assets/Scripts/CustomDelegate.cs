﻿namespace Assets.Scripts
{
    public class CustomDelegate
    {
        public delegate void StateChangedDelegate(StateChangedArgs args);
    }

    public class StateChangedArgs
    {
        public string OldState { get; set; }
        public string NewState { get; set; }
    }
}
