﻿using Assets.Scripts.Extensions;
using Pathfinding;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class Mover
    {
        #region Inspector

        [SerializeField]
        private Transform target;

        [SerializeField]
        private float minDeltaDistance;

        #endregion

        #region Properties

        private float fleeSpeed => _npc.Speed * _npc.Stats.FleeSpeedMult;
        private NPCStats stats => _npc.Stats;
        public Transform Target => _npc.Target;
        public Vector2 Position => _npc.transform.position;
        public Vector3 DesiredVelocity => _path?.desiredVelocity ?? Vector3.zero;

        #endregion

        #region Private fields

        private AIDestinationSetter _aids;
        private AIPath _path;
        private MoveMode _mode;
        private NPC _npc;
        private float timeStuck;
        private float _remainingDistance;
        private Transform _fleeFromTarget;
        private float _deltaDistance;
        private Rigidbody2D _rb;

        #endregion

        public Mover(NPC npc)
        {
            _npc = npc;
            _aids = _npc.GetComponent<AIDestinationSetter>();
            _aids.target = npc.Target;
            _path = _npc.GetComponent<AIPath>();
            _path.orientation = OrientationMode.YAxisForward;
            _path.enableRotation = false;
            _rb = _npc.GetComponent<Rigidbody2D>();
            minDeltaDistance = 0.01f;
        }

        #region IState implementation

        private void OnEnter()
        {
            _path.enabled = true;
            _aids.enabled = true;
            Stop();
            _npc.Target.gameObject.SetActive(true);
        }

        public void OnExit()
        {
            _path.enabled = false;
            _aids.enabled = false;
            Stop();
            _npc.Target.gameObject.SetActive(false);
        }

        public void Tick()
        {
            _deltaDistance = _remainingDistance;
            _remainingDistance = GetRemainingDistance();
            _deltaDistance = Mathf.Abs(_deltaDistance - _remainingDistance);
            if(_deltaDistance < minDeltaDistance)
            {
                //_npc.Target.position = _npc.transform.position;
            }

            switch (_mode)
            {
                case MoveMode.FOLLOW:
                    {
                        if (_remainingDistance < stats.CurioInteractionDistance)
                            _npc.Target.gameObject.SetActive(false);
                        break;
                    }
                case MoveMode.FLEE:
                    {
                        if (_deltaDistance < minDeltaDistance)
                        {
                            RunFrom(_fleeFromTarget);
                            //_path.Teleport(_npc.transform.position)
                        }
                        if (_remainingDistance < minDeltaDistance)
                        {
                            RunFrom(_fleeFromTarget);
                        }
                        break;
                    }
            }
        }

        #endregion

        #region Methods

        public float GetRemainingDistance()
        {
            var dist = Vector2.Distance(_aids.target.position, _npc.transform.position);
            return Mathf.Abs(dist);
        }

        public void SetTarget(Transform transform, MoveMode moveMode)
        {
            if (transform == null)
                return;

            target = transform;
            _mode = moveMode;

            if(moveMode == MoveMode.FOLLOW)
            {
                RunTowards(transform);
            }
            else if(moveMode == MoveMode.FLEE)
            {
                RunFrom(transform);
            }
            OnEnter();
        }

        public void Stop()
        {
            _rb.velocity = Vector2.zero;
        }

        public void RunTowards(Transform target)
        {
            _aids.target = target;
            _path.maxSpeed = _npc.Speed;
            _fleeFromTarget = null;
        }

        public void RunFrom(Transform other)
        {
            _fleeFromTarget = other;
            var vec = GetFleeTarget(other);
            _npc.Target.position = vec;
            
            _aids.target = _npc.Target;
            _path.maxSpeed = fleeSpeed;
        }

        public Vector2 GetFleeTarget(Transform other)
        {
            Vector2 direction = (_npc.transform.position - other.position).normalized;
            var angle = Random.Range(-stats.FleeMaxAngle, stats.FleeMaxAngle);
            var distance = Random.Range(stats.MinFleeDistance, stats.MaxFleeDistance);
            var vec = direction.Rotate(angle) * distance;
            //zamien na najblizsza valid pozycje
            return vec;
        }

        #endregion
    }

    public enum MoveMode
    {
        NONE,
        FOLLOW,
        FLEE
    }
}
