﻿using System;
using UnityEngine;
using static Assets.Scripts.Delegates;

namespace Assets.Scripts
{
    public abstract class ScheduleItem : IState
    {
        #region Properties

        private float duration;
        public float Duration => duration;

        private bool _isRunning;
        public bool IsRunning => _isRunning;

        private float _startTime;

        public ScheduleItem Next { get; set; }

        protected Notification OnEnterNotification;
        protected Notification OnExitNotification;
        #endregion

        public void OnEnter()
        {
            _isRunning = true;
            _startTime = Time.time;
            OnEnterNotification?.Invoke();
        }

        public void OnExit()
        {
            _isRunning = false;
            OnExitNotification?.Invoke();
        }

        //TODO zmienic na Set albo zmienic typ na buildera
        public ScheduleItem WithDuration(float duration)
        {
            this.duration = duration;
            return this;
        }

        private Vector2? _position;
        public Vector2? Position => _position;

        public ScheduleItem WithLocation(Vector2 position)
        {
            this._position = position;
            return this;
        }

        public abstract void Tick();

        public float RemainingTime()
        {
            if (_isRunning)
            {
                var remaining = _startTime + duration - Time.time;
                return remaining > 0 ? remaining : 0;
            }
            return 0;
        }

        private bool CheckLocation()
        {
            if (_position.HasValue)
            {
                return true;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Override this method to add custom condition required to determine if Schedule Item is finished
        /// </summary>
        /// <returns></returns>
        protected bool AdditionalFinishCondition() => true;

        private bool IsTimeUp() => RemainingTime() <= 0;

        public Func<bool> Finished => () => !_isRunning || (IsTimeUp() && CheckLocation() && AdditionalFinishCondition());
    }
}
