using Assets.Scripts.Templates;
using Pathfinding;
using System;
using UnityEngine;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts
{
    [RequireComponent(typeof(AIDestinationSetter))]
    [RequireComponent(typeof(AIPath))]
    public class NPC : Actor, IMovable
    {
        #region Inspector
        
        [SerializeField]
        private NPCTemplate Template;

        #endregion

        #region Properties
        public IInteractable Interactable { get; set; }

        public NPCStats Stats { get; private set; }

        #endregion

        #region Private fields

        private StateMachine _stateMachine;

        #endregion

        void Start()
        {
            Setup();   
        }

        void Update()
        {
            _stateMachine?.Tick();
        }

        #region Setup

        private void Setup()
        {
            InitTemplate();
            InitComponents();
            InitPathfinding();
            SetupStateMachine();
        }

        private void InitPathfinding()
        {
            Target = PathfindingManager.GetPathfindingTargetFor(this);
            Mover = new Mover(this);
        }

        private void InitTemplate()
        {
            Name = Template.Name;
            Speed = Template.Stats.Speed;
            Stats = Instantiate(Template.Stats);
        }

        private void SetupStateMachine()
        {
            _stateMachine = new StateMachineBuilder(this).Build();
            _stateMachine.OnStateChanged += LogStateMachine;
        }

        #endregion

        #region State conditions

        public Func<bool> HasTarget() => () => Target.gameObject.activeInHierarchy;
        public Func<bool> HasInteractionTarget() => () => Interactable != null;
        public Func<bool> HasNoInteractionTarget() => () => Interactable == null;
        //public Func<bool> IsAngry() => () => angry;
        //public Func<bool> HasNothingToDo() => () => hasNothingToDo;

        #endregion

        #region Private methods

        private void LogStateMachine(StateChangedArgs args)
        {
            Logger?.Log($"{name}: changing state: {args.OldState} => {args.NewState}", this);
        }

        private void OnDrawGizmos()
        {
            var color = Gizmos.color;
            if (Stats != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, Stats.FleeDistance);
            }
            if(Mover != null)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(transform.position, transform.position + Mover.DesiredVelocity);
            }
            Gizmos.color = color;
        }

        #endregion
    }
}
