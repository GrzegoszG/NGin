﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class CommandProcessor
    {
        private Queue<ICommand> _commands;
        private ICommand _currentCommand;

        public CommandProcessor()
        {
            _commands = new Queue<ICommand>();
        }

        public CommandProcessor(IEnumerable<ICommand> commands)
        {
            _commands = new Queue<ICommand>(commands);
        }

        private void ProcessCommands()
        {
            if(_currentCommand != null && !_currentCommand.IsFinished)
            {
                return;
            }

            if(!_commands.Any())
            {
                return;
            }

            _currentCommand = _commands.Dequeue();
            _currentCommand.Execute();
        }
    }
}
