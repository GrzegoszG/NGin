﻿using Assets.Scripts.Entities.Items;
using Assets.Scripts.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Entities.Curios
{
    public class Chest : Curio, IContainer
    {
        private List<IItem> _items;

        public List<IItem> GetItems()
        {
            return _items.ToList();
        }

        public override void OnInteractionStart()
        {
            //show UI with items?
        }

        public bool PutItem(IContainer who, IItem item)
        {
            return true;
        }

        public bool TakeItem(IContainer who, IItem item)
        {
            return true;
        }
    }
}
