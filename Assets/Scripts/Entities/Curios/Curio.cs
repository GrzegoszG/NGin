﻿using UnityEngine;

namespace Assets.Scripts.Entities.Curios
{
    [RequireComponent(typeof(Animation))]
    [RequireComponent(typeof(SpriteRenderer))]
    public abstract class Curio : MonoBehaviour, IInteractable
    {
        #region Inspector

        [SerializeField]
        private ParticleSystem _particleSystem;

        [SerializeField]
        private AnimationClip _animClip;

        private Animation _animation;

        #endregion

        #region Properties

        private bool _readyToUse;
        public bool ReadyToUse
        {
            get { return _readyToUse; }
            set { _readyToUse = value; }
        }

        private CustomLogger _logger;
        public CustomLogger Logger => _logger;

        #endregion

        #region Private fields

        

        #endregion

        private void Start()
        {
            _readyToUse = true;
            _animation = GetComponent<Animation>();
        }

        #region Interface implementation

        public void Interact(IInteractor interactor)
        {
            if (!ReadyToUse) return;

            LogInteraction(interactor);
            OnInteractionStart();
            PlayEffects();
        }

        public abstract void OnInteractionStart();

        private void PlayEffects()
        {
            if(_animClip != null)
            {
                _animation.clip = _animClip;
                _animation.Stop();
                _animation.Play();
            }
            if(_particleSystem != null)
            {
                _particleSystem.Play();
            }
        }

        #endregion

        #region Methods
        private void LogInteraction(IInteractor interactor)
        {
            Logger?.Log($"{name}: interacting with: {interactor.InteractorName}!", this);
        }

        #endregion
    }
}
