﻿using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.States
{
    public class Routine : AIState
    {
        private NPC _npc;
        private CommandProcessor commandProcessor;

        private RoutineMachine routineMachine;
        private ScheduleItem _recentActivity;

        public RoutineStartMode StartMode;

        private List<ScheduleItem> _items;

        public Routine(NPC npc, List<ScheduleItem> items)
        {
            _npc = npc;
            _items = items;
            routineMachine = new RoutineMachine(items.FirstOrDefault(), _npc.Mover);
            AddTransitions();
        }

        private void AddTransitions()
        {
            InitChain();
            _items.ForEach(i => routineMachine.AddTransition(i, i.Next, i.Finished));
        }

        private void InitChain()
        {
            for (int i = 0; i < _items.Count - 1; i++)
            {
                _items[i].Next = _items[i + 1];
            }
            _items.Last().Next = _items[0];
        }

        public override void OnEnter()
        {
            var item = DetermineStartingItem();
            routineMachine.SetState(item);
        }

        public override void OnExit()
        {
            _recentActivity = routineMachine.CurrentState; 
        }

        public override void Tick()
        {
            routineMachine?.Tick();
        }

        #region Methods

        private ScheduleItem DetermineStartingItem()
        {
            switch (StartMode)
            {
                case RoutineStartMode.Reset:
                    return routineMachine.InitialState;
                case RoutineStartMode.Continue:
                    return routineMachine.PreviousState;
            }
            return routineMachine.InitialState;
        }

        #endregion
    }

    public enum RoutineStartMode
    {
        Reset,
        Continue
    }
}
