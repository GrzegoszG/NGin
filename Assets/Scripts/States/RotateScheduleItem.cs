﻿using System;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class RotateScheduleItem : ScheduleItem
    {
        private readonly Transform _body;
        public float RotateSpeed { get; }
        private bool Clockwise { get; }
        public RotateScheduleItem(Transform npcBody, bool clockWise, float rotateSpeed = 180)
        {
            _body = npcBody;
            RotateSpeed = Math.Abs(rotateSpeed);
            Clockwise = clockWise;
            OnEnterNotification = ResetRotation;
            OnExitNotification = ResetRotation;
        }

        private void ResetRotation()
        {
            _body.rotation = Quaternion.identity;
        }

        public override void Tick()
        {
            if(RotateSpeed < 0)
            {

            }
            _body.Rotate(Clockwise ? Vector3.back : Vector3.forward, RotateSpeed * Time.deltaTime);
        }
    }
}
