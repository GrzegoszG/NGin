﻿using Assets.Scripts.Entities.Curios;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.States
{
    public class SearchForCurio : AIState
    {
        private NPC _npc;
        private Curio _curio;

        public SearchForCurio(NPC npc)
        {
            _npc = npc;
        }

        public override void OnEnter()
        {

        }

        public override void OnExit()
        {
            if (_curio != null)
            {
                _npc.Target.position = _curio.transform.position;
                _npc.Target.gameObject.SetActive(true);
            }
        }

        public override void Tick()
        {
            _curio = FindOtherNearestCurio();
            _npc.Interactable = _curio;
        }

        private Curio FindOtherNearestCurio(int pickFromNearest = 3)
        {
            var curio = UnityEngine.Object.FindObjectsOfType<Curio>()
             .OrderBy(t => Vector3.Distance(_npc.transform.position, t.transform.position))
             .Where(t => t.ReadyToUse)
             .Take(pickFromNearest)
             .OrderBy(t => UnityEngine.Random.Range(0, int.MaxValue))
             .FirstOrDefault();

            return curio;
        }
    }
}
