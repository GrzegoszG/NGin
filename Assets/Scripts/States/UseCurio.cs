﻿using Assets.Scripts.Entities.Curios;

namespace Assets.Scripts.States
{
    public class UseCurio : AIState
    {
        private NPC _npc;

        public UseCurio(NPC npc)
        {
            _npc = npc;
        }

        public override void OnEnter()
        {

        }

        public override void OnExit()
        {
            _npc.Interactable = null;
        }

        public override void Tick()
        {
            if(_npc.Interactable is Curio)
            {
                Use((Curio)_npc.Interactable);
            }
            _npc.Interactable = null;
        }

        private void Use(Curio curio)
        {
            if (curio != null && curio.enabled && curio.isActiveAndEnabled && curio.ReadyToUse)
                _npc.InteractWith(curio);
        }
    }
}
