﻿namespace Assets.Scripts.States
{
    public abstract class AIState : IState
    {
        public abstract void OnEnter();

        public abstract void OnExit();

        public abstract void Tick();
    }
}
