﻿using Assets.Scripts;
using Assets.Scripts.States;

internal class MoveToTarget : AIState
{
    private readonly Mover _mover;
    
    public MoveToTarget(Mover mover)
    {
        _mover = mover;
    }
    
    public override void Tick() => _mover.Tick();

    public override void OnEnter() => _mover.SetTarget(_mover.Target, MoveMode.FOLLOW);

    public override void OnExit() => _mover.OnExit();
}