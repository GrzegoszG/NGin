﻿using Assets.Scripts.Entities.Curios;

namespace Assets.Scripts.States
{
    public class UseCurioLongActivity : ScheduleItem
    {
        private Curio _curio;
        private NPC _npc;

        public UseCurioLongActivity(NPC npc)
        {
            _npc = npc;
        }

        public void SetTarget(Curio curio)
        {
            _curio = curio;
        }

        public new void OnEnter()
        {
            base.OnEnter();
        }

        public new void OnExit()
        {
            base.OnExit();
        }

        public override void Tick()
        {
            if (_curio == null) return;

            _npc.InteractWith(_curio);
        }
    }
}
