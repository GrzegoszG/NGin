﻿using Assets.Scripts;
using Assets.Scripts.States;
using System;
using System.Linq;
using UnityEngine;

public class Flee : AIState
{
    private NPC _npc;

    public Flee(NPC npc)
    {
        _npc = npc;
    }

    public override void OnEnter()
    {
        _npc.Mover.SetTarget(GetNearestThreat(), MoveMode.FLEE);
    }

    public override void Tick()
    {
        _npc.Mover.Tick();
    }

    public override void OnExit()
    {
        _npc.Mover.OnExit();
    }

    #region Methods

    public Func<bool> IsScaredCondition() => () => IsScared();

    public bool IsScared()
    {
        var threat = GetNearestThreat();
        if (threat != null)
        {
            if (Vector3.Distance(threat.position, _npc.transform.position) < _npc.Stats.FleeDistance)
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }

    public Transform GetNearestThreat()
    {
        var enemy = GameObject.FindObjectsOfType<Guild>()?
                                .Where(g => g.IsScary)
                                .OrderBy(o => Vector3.Distance(o.transform.position, _npc.transform.position))
                                .FirstOrDefault();

        if (enemy != null)
        {
            return enemy.transform;
        }
        else
            return null;
    }

    #endregion
}