﻿using Assets.Scripts.Entities.Curios;

namespace Assets.Scripts.States
{
    public class Interact : IState
    {
        private Curio _target;
        private IInteractor _interactor;

        public void SetTarget(Curio curio)
        {
            _target = curio;
        }

        public Interact(IInteractor interactor)
        {
            _interactor = interactor;
        }

        public void OnEnter()
        {
          
        }

        public void OnExit()
        {
           
        }

        public void Tick()
        {
           if(_target.ReadyToUse)
           {
                _target.Interact(_interactor);
           }
        }
    }
}
